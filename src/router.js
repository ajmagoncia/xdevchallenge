import Vue from 'vue'
import Router from 'vue-router'
import firstChallenge from './components/firstChallenge.vue'
import secondChallenge from './components/secondChallenge.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'first-challenge',
      component: firstChallenge
    },
    {
      path: '/second',
      name: 'second-challenge',
      component: secondChallenge
    }
  ]
})